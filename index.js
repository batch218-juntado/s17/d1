console.log("Hello World");

// [SECTION] Functions
// Functions in Javascript are lines/block of codes that tell our device/application to perform a certain task when called/invokes
// They are also used to prevent repeating lines/block of codes that perform the same task/function.

// Function Declaration
	// function statement defines a function with parameters

	// Function Declaration aka Function Keyword - is used to define a Javascript function
	// Function Name - is used so we are able to call/invoke a declared function
	// Function Code Block ({}) - the statement which compromise the body of the function. This is where the code to be executed.

// Inside the function declaration -> Function Name - requires open and close parenthesis
function printName(){ //code block
		console.log("My name is John"); //function statement
}; //delimeter

printName(); // Function Invocation


	// [HOISTING] - is Javascript's behavior for certain variables and functions to run or use before their declaration. 
	// Make sure the function is existing whenever we call/invoke a function
declaredFunction();


	function declaredFunction(){
		console.log("Hello world");
	}

declaredFunction();

// [Function Expression] - A function can be also stored in a variable. That is called as function expression
// Hoisting is allowed in function declaration, however, we could not hoist through function expression
let variableFunction = function(){
	console.log("Hello again!");
};

variableFunction();

let funcExpression=function funcName(){
	console.log("Hello from the other side.");
}

funcExpression();
// funcName(); -> Whenever we are calling a function stored in a variable, we just call the variable name, not the function name

console.log("-----------");
console.log("[Reasigning Functions]");

declaredFunction();

// [Re-declare] this is just a border
declaredFunction=function(){
	console.log("updated declaredFunction")
}

declaredFunction();

// [Re-declare] this is just a border
declaredFunction=function(){
	console.log("updated funcExpression")
}

declaredFunction();

// Constant Function
const constantFunction=function(){
	console.log("Initialized with const.");
}

constantFunction();

/*
constantFunction=function(){
	console.log("New value");
}

constantFunction();

// Function with const keyword cannot be reassigned.
*/

// Scope is the accessibility/visivility of variables in the code

/*
	Javascript variables has 3 types of scope:
	1. local/block scope
	2. globals scope
	3. function scope

*/

console.log("---------------");
console.log("[Function Scoping]");

{
	let localVar="Armando Perez";
	console.log(localVar)
}

// console.log(localVar)

let globalVar="Mr. Worldwide";
console.log(globalVar);

{
console.log(globalVar);
}

// [Function Scoping]
// Variables defined inside a function are not accessible/visible outside the function
// Variables declared with var, let, and const are quite similar when declared inside a function

function showNames(){
	var functionVar="Joe";
	const functionConst="John";
	let functionLet="Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();

/*
Error - these are functions scoped variable and cannot be accessed outside the function they were declared. {}

console.log(functionVar);
console.log(functionConst);
console.log(functionLet);*/


// [Nested Function] - You can create other functions inside a function, called Nested Function

function myNewFunction(){
	let name="Jane";
	function nestedFunction(){
		let nestedName="John";
		console.log(name);
	}

	nestedFunction(); // result to not defined error

	console.log(nestedFunction);
}

myNewFunction();

// Global Scoped Variable
let globalName="Zuitt";

function myNewFunction2(){
	let nameInside="Renz";
	console.log(globalName)
}

myNewFunction2();


// alert() - allows us to show a small window at the top of our browser page to show information to our users. can be used when to inform a user that they made a mistake
// alert("Sample Alert");

function showSampleAlert(){
	alert("Hello, User");
}

showSampleAlert();

// alert messages inside a function will only execute whenever we call/invoke the function

console.log("I will only log in the console when the alert is dismissed");

// ^ this wont appear immediately. alert will show first

/* Notes on the use of alert() 
	- Show only an alert for short dialogs/messages to the user 
	- Do not overuse alert() bc the program/js has to wait for it to be dismissed before continuing.
	*/

// [Prompt] - prompt() allow us to show the small window at the top of the browser to gather user input.
	
/*	let samplePrompt=prompt("Enter your Full Name");
	
	// console.log(samplePrompt);

	console.log(typeof samplePrompt); // string

	console.log("Hello, " + samplePrompt); // coercion concuttination
	*/


	function printWelcomeMessage(){
		let firstName=prompt("Enter your first name: ");
		let lastName=prompt("Enter your last name: ");

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!");
	}

	printWelcomeMessage();


// Storing Arrays

	// [SECTION] Function Naming Convention

	// Function name should be definitive of the task it will perform. It usually contains a verb.

	function getCourses(){
		let courses=["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}

	getCourses(); // the function of this is to get the courses. 

	// Avoid generic names to avoid confusion within our code.

	function get(){
		let name="Jamie";
		console.log(name);
	}

	get();

	// Avoid pointless and inappropriate function names, example: foo, bar, etc.

	function foo(){
		console.log(25%5);
	}

	foo();

	// Name your function in small caps. Follow camelCase when naming variables and functions

	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");
	}

	displayCarInfo();